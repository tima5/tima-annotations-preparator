# paths

path_r_python = function(x, y = NULL) { 
  if (!is.null(y)) { 
    if (language == "r") { 
      path = file.path(x, y)
    }
    if (language == "python") {
      path = os.pathjoin(x, y)
    }
  return(path)
  }
  if (is.null(y)) { 
    if (language == "r") { 
      path = file.path(x)
    }
    if (language == "python") {
      path = os.pathjoin(x)
    }
    return(path)
  }
}

## config
config = path_r_python("../config")

### default

config_default = path_r_python(config, "default")

#### treat ISDB

config_default_treat_isdb = path_r_python(config_default, "treat_isdb_default.yaml")

#### treat GNPS NAP

config_default_treat_gnps_nap = path_r_python(config_default, "treat_gnps_nap_default.yaml")

#### fill chemical

config_default_fill_chemical = path_r_python(config_default, "fill_chemical_default.yaml")

#### fill features

config_default_fill_features = path_r_python(config_default, "fill_features_default.yaml")

### params

config_params = path_r_python(config, "params")

#### treat ISDB

config_params_treat_isdb = path_r_python(config_params, "treat_isdb_params.yaml")

#### treat GNPS NAP

config_params_treat_gnps_nap = path_r_python(config_params, "treat_gnps_nap_params.yaml")

#### fill chemical 

config_params_fill_chemical = path_r_python(config_params, "fill_chemical_params.yaml")

#### fill features 

config_params_fill_features = path_r_python(config_params, "fill_features_params.yaml")

## data

data = path_r_python("../data")

### source

data_source = path_r_python(data, "source")

#### databases

data_source_databases = path_r_python(data_source, "databases")

##### lotus

data_source_databases_file = path_r_python(data_source_databases, "db_prepared.tsv.gz")

### interim

data_interim = path_r_python(data, "interim")

#### structures

data_interim_structures = path_r_python(data_interim, "structures")

##### original

data_interim_structures_original = path_r_python(data_interim_structures, "original.tsv.gz")

##### cleaned

data_interim_structures_cleaned = path_r_python(data_interim_structures, "cleaned.tsv.gz")

### processed

data_processed = path_r_python(data, "processed")

#### annotations

data_processed_annotations = path_r_python(data_processed, "annotations")

#### params 

data_processed_params = path_r_python(data_processed, "params")

##### treat isdb

data_processed_params_treat_isdb = path_r_python(data_processed_params, "treat_isdb_params.yaml")

##### treat isdb

data_processed_params_treat_gnps_nap = path_r_python(data_processed_params, "treat_gnps_nap_params.yaml")

##### fill chemical

data_processed_params_fill_chemical = path_r_python(data_processed_params, "fill_chemical_params.yaml")

##### fill features
data_processed_params_fill_features = path_r_python(data_processed_params, "fill_features_params.yaml")

## src

### rdkit script

rdkit_script = "python/chemosanitizer.py"

### docopt

#### treat isdb

docopt_treat_isdb = "docopt/treat_isdb.txt"

#### treat gnps_nap
docopt_treat_gnps_nap = "docopt/treat_gnps_nap.txt"

#### fill chemical classification
docopt_fill_chemical = "docopt/fill_chemical.txt"

#### fill chemical classification
docopt_fill_features = "docopt/fill_features.txt"

