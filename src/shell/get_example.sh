#!/usr/bin/env bash
if [ ! -f LICENSE ]; then
  echo "Sorry, you need to run that from the root of the project."
  exit 1
fi

mkdir -p data/source/
wget "https://metabo-store.nprod.net/tima_example_files/interim/example_isdb_result.tsv.gz" -O data/source/example_isdb_result.tsv.gz
