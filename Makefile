include config.mk

.PHONY: get-example get-lotus fill-chemical fill-features treat-gnps-nap treat-isdb
.PRECIOUS: %.tsv %.zip %.json %.gz

get-example:
	bash src/shell/get_example.sh

get-lotus: 
	bash src/shell/get_lotus.sh

treat-gnps-nap:
	cd src && Rscript R/treat_gnps_nap.R

treat-isdb:
	cd src && Rscript R/treat_isdb.R

fill-chemical:
	cd src && Rscript R/fill_chemical.R

fill-features:
    	cd src && Rscript R/fill_features.R