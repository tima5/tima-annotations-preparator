#!/usr/bin/env bash

echo "Let's check if we can run the whole process"
echo " Get lotus"
make get-lotus
echo " Treat GNPS results (fake for now)"
make treat-gnps-nap
echo " Get example result from ISDB"
make get-example
echo " Treat ISDB results"
make treat-isdb
echo " Fill missing chemical data"
make fill-chemical
echo " Fill component ids"
make fill-features

echo "We need to do quality controls, please help!"
